-author: Eric Zhou

-contact address email: yixiongz@uoregon.edu

-description: A web based local ACP Brevet Control Times Calculator calculator with a database
It receive the input data from frontend, send the data to backend by AJAX, after solve the data
the result by be sent back by the same way. When click the submit button, data from input will be stored in to, when click the display button, the data which are stored in the database will be displayed on the browser.


-reference:https://rusa.org/pages/acp-brevet-control-times-calculator
		   https://rusa.org/octime_acp.html
