import os
import flask
import arrow
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import acp_times
import config
import logging

app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

db_array = []

@app.route('/todo', methods=['POST'])
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]
    if items == []:
        return render_template('unsubmit.html', items = items)
    else:
        return render_template('todo.html', items = items)



@app.route('/new', methods=['POST'])
def new():
    global db_array
    size = len(db_array)
    for i in range(0, int(size), 4):
        item_doc = {
            'get_brevet_km': db_array[i],
            'arrow_time': db_array[i+1],
            'open_time': db_array[i+2],
            'close_time': db_array[i+3]
        }
        db.tododb.insert_one(item_doc)
    return redirect(url_for('index'))

    
@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    get_brevet_km = request.args.get('brevet_km', 999, type= int)
    get_begin_date = request.args.get('begin_date', '', type= str)
    get_begin_time = request.args.get('begin_time', '', type= str)


    format_time = get_begin_date + 'T' + get_begin_time + ':00.000000-08:00'
    arrow_time = arrow.get(format_time)

    open_time = acp_times.open_time(km, get_brevet_km, arrow_time)
    close_time = acp_times.close_time(km, get_brevet_km, arrow_time)
    result = {"open": open_time, "close": close_time}
    
    global db_array
    db_array.append(get_brevet_km)
    db_array.append(arrow_time.isoformat())
    db_array.append(open_time)
    db_array.append(close_time)
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")

